
%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUFFER_LENGTH 256

global _start
section .bss
buffer: resb BUFFER_LENGTH

section .data
overflow_error: db "Input message is too long", 0
key_not_found_error: db "Key wasn't found", 0

section .text

_start:
    mov rdi, buffer
    mov rsi, BUFFER_LENGTH
    
    call read_word

    test rax, rax
    
    jz .overflow

    mov rdi, rax
    mov rsi, pointer

    call find_word

    test rax, rax
    jz .not_found

    mov rdi, rax
    push rdi
    add rdi, 8
    call string_length
    pop rdi
    add rdi, rax
    add rdi, 9
    call print_string
    call print_newline
    call exit

    .overflow:
        mov rdi, overflow_error
        call print_error
        call print_newline
        call exit

    .not_found:
        mov rdi, key_not_found_error
        call print_error
        call print_newline
        call exit