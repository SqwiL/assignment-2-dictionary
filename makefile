ASM=nasm 
ASMFLAGS=-f elf64 
LD= ld 

.PHONY:
	clean

clean:
	rm *.o

main: main.o lib.o dist.o
	ld -o main $^
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<